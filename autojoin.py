from collections import defaultdict
import json
import os

import terminaltables
import weechat

"""
* Adds /autojoin
"""

weechat.register(
    # name
    'autojoin',
    # author
    'fuze',
    # version
    '1.0',
    # license
    'GPL3',
    # description
    'Autojoin channels',
    # shutdown function
    '',
    # charset
    ''
)

# autojoin init
# -------------
weechat.hook_signal('*,irc_in_invite', 'invite_callback', '')

weechat.hook_command(
    # command
    'autojoin',
    # description
    'Sets a channel to be auto joined',
    # args
    '<list|remove|<network> <channel> [channel key]>',
    # args description
    '',
    # completion
    '',
    # callback
    'autojoin',
    # callback data
    ''
)

CONFIG_FILE = os.path.join(
    weechat.info_get('weechat_dir', ''),
    'python/autojoin.json'
)
AUTOJOIN_CHANNELS = defaultdict(dict)


def get_autojoin_channels():
    with open(CONFIG_FILE, 'r') as f:
        try:
            config_data = json.load(f)
        except (IOError, ValueError):
            weechat.prnt('', 'No autojoin channels')
            return

    for network, channel_data in config_data.iteritems():
        for channel_name in channel_data:
            AUTOJOIN_CHANNELS[network][channel_name] = channel_data[channel_name]

    weechat.prnt('', 'Loaded autojoin channels')


def print_autojoin_channels():
    weechat.prnt('', 'Autojoin channels:')

    table_data = []
    for network in AUTOJOIN_CHANNELS:
        table_data.append(['Channel', 'Key'])
        channels = AUTOJOIN_CHANNELS[network].keys()
        channels.sort()
        for channel in channels:
            chan_key = AUTOJOIN_CHANNELS[network][channel].get('key', '')
            table_data.append([channel, chan_key])
        table = terminaltables.AsciiTable(table_data)
        table.title = network
        weechat.prnt('', table.table)

        table_data = []


def add_autojoin_channel(network, channel):
    try:
        channel_name, channel_key = channel.split()
    except ValueError:
        channel_name, channel_key = channel.split()[0], None

    if channel_name in AUTOJOIN_CHANNELS[network]:
        weechat.prnt(
            '',
            'Channel {} for network {} is already added!'.format(
                channel_name, network
            )
        )
        return

    channel_key = {'key': channel_key} if channel_key else {}
    AUTOJOIN_CHANNELS[network].update({channel_name: channel_key})

    save_config()

    weechat.prnt(
        '', 'Successfully added {} to autojoin list!'.format(channel_name)
    )


def remove_autojoin_channel(network, channel):
    if channel not in AUTOJOIN_CHANNELS[network]:
        weechat.prnt(
            '',
            'Channel {} for network {} is not currently set to autojoin'.format(
                channel, network
            )
        )
        return

    del AUTOJOIN_CHANNELS[network][channel]
    if not AUTOJOIN_CHANNELS[network]:
        del AUTOJOIN_CHANNELS[network]
    save_config()

    weechat.prnt(
        '', 'Successfully removed {} from autojoin channels'.format(channel)
    )


def save_config():
    with open(CONFIG_FILE, 'wb') as f:
        json.dump(AUTOJOIN_CHANNELS, f, sort_keys=True, indent=4)


def autojoin(data, gui_buffer, args):
    usage_string = 'usage: /autojoin <list|remove|<network> <channel> [channel key]>'
    if not args:
        weechat.prnt('', usage_string)
    else:
        args = args.split()
        args_length = len(args)
        if args_length < 1:
            weechat.prnt('', usage_string)
        elif args_length == 1 and args[0] == 'list':
            print_autojoin_channels()
        elif args_length == 3 and args[0] == 'remove':
            remove_autojoin_channel(args[1], args[2])
        elif args_length == 3:
            network = args[0]
            channel = ' '.join(args[1:])
            add_autojoin_channel(network, channel)
        else:
            weechat.prnt('', usage_string)

    return weechat.WEECHAT_RC_OK


def invite_callback(data, signal, signal_data):
    network, _ = signal.split(',')
    channel = signal_data.split(':')[-1]

    if channel in AUTOJOIN_CHANNELS[network]:
        chan_key = AUTOJOIN_CHANNELS[network][channel].get('key')
        if chan_key:
            channel += ' {}'.format(chan_key)

        weechat.command(
            '',
            '/join -noswitch -server {} {}'.format(network, channel)
        )

    return weechat.WEECHAT_RC_OK

get_autojoin_channels()

